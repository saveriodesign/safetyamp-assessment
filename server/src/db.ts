import { Sequelize, ModelCtor, DataTypes } from 'sequelize';

import { Task, Hazard } from './models/models-module';

export class DbService {

  /** The database connection */
  public db: Sequelize;

  /** "hazards" table model */
  public HazardModel: ModelCtor<any>;
  /** "tasks" table model */
  public TaskModel: ModelCtor<any>;

  /**
   * @param user Postgres access username
   * @param pass Postgres access password
   * @param uri DB address(:port)
   */
  constructor (user: string, pass: string, uri: string) {
    this.db = new Sequelize(`postgres://postgres:postgres@localhost:5432/jha-db`);
    // this.db = new Sequelize('jha-db', user, pass, {
    //   host: uri,
    //   dialect: 'postgres'
    // });
    this.HazardModel = this.db.define('hazard', Hazard.attributes, Hazard.options);
    this.TaskModel = this.db.define('task', Task.attributes, Task.options);
    // associate each hazard with a task
    this.TaskModel.hasMany(this.HazardModel);
    this.HazardModel.belongsTo(this.TaskModel);
  }

  /** Synchronize models with DB */
  public async init (): Promise<void> {
    try {
      await this.db.sync();
      return console.log('Synchronized with DB');
    }
    catch (error) {
      console.error('Synchronization failed:');
      console.error(error);
      return Promise.reject();
    }
  }

}
