/**
 * Arguments from CLI
 * 
 * Required:  
 * `u | user . . .` DB username  
 * `p | pass . . .` DB password  
 * `a | address ..` DB address
 */
const argv: any = require('minimist')(process.argv.slice(2));

/** Show usage help in CLI */
function displayHelp(): void {
  console.log('\nRequired options\n');
  console.log('  u, user       Database user name');
  console.log('  p, pass       Database user password');
  console.log('  a, address    Database uri (and :port if necessary)');
  console.log('\nOther options\n');
  console.log('  h, help       Display this message');
  console.log('\nExit codes\n');
  console.log('  1             Bad input');
  console.log('  2             Database sync failure');
}

// Exit if app doesn't have enough input to start up
if (argv.h || argv.help) {
  displayHelp();
  process.exit(0);
}

if (
  !argv.u && !argv.user ||
  !argv.p && !argv.pass ||
  !argv.a && !argv.address
) {
  displayHelp();
  process.exit(1);
}

/** DB username */
const U: string = argv.u || argv.user;
/** DB user password */
const P: string = argv.p || argv.pass;
/** DB address(:port) */
const A: string = argv.a || argv.address;

import { DbService } from './db';

const tasks: Array<any> = require('../../data/tasks');
const hazards: Array<any> = require('../../data/hazards');

/** Database connection */
const database = new DbService(U, P, A);

// intialize database and listen for requests
database.init()
  .then(() => Promise.all(tasks.map((task) => database.TaskModel.create(task))))
  .then(() => Promise.all(hazards.map((hazard) => database.HazardModel.create(hazard))))
  .then(() => {
    console.log('\nInitial data loaded into database\n');
    process.exit(0);
  })
  .catch(() => process.exit(2));
