/**
 * Arguments from CLI
 * 
 * Required:  
 * `u | user . . .` DB username  
 * `p | pass . . .` DB password  
 * `a | address ..` DB address
 */
const argv: any = require('minimist')(process.argv.slice(2));

/** Show usage help in CLI */
function displayHelp(): void {
  console.log('\nRequired options\n');
  console.log('  u, user       Database user name');
  console.log('  p, pass       Database user password');
  console.log('  a, address    Database uri (and :port if necessary)');
  console.log('\nOther options\n');
  console.log('  h, help       Display this message');
  console.log('\nExit codes\n');
  console.log('  1             Bad input');
  console.log('  2             Database sync failure');
}

// Exit if app doesn't have enough input to start up
if (argv.h || argv.help) {
  displayHelp();
  process.exit(0);
}

if (
  !argv.u && !argv.user ||
  !argv.p && !argv.pass ||
  !argv.a && !argv.address
) {
  displayHelp();
  process.exit(1);
}

/** DB username */
const U: string = argv.u || argv.user;
/** DB user password */
const P: string = argv.p || argv.pass;
/** DB address(:port) */
const A: string = argv.a || argv.address;

import { Application } from 'express';
import * as bodyParser from 'body-parser';
import * as http from 'http';

import { DbService } from './db';

const express: any = require('express');
const finale: any = require('finale-rest'); // no @types/... for this module

/** Database connection */
const database = new DbService(U, P, A);
/** Express application */
const app: Application = express();

// setup server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(require('cors')());
/** HTTP server */
const server = http.createServer(app);

// init finale
finale.initialize({
  app: app,
  sequelize: database.db
});

// setup REST resources
const taskResource = finale.resource({
  model: database.TaskModel,
  endpoints: ['/d/tasks', '/d/tasks/:id']
});
const hazardResource = finale.resource({
  model: database.HazardModel,
  endpoints: ['/d/hazards', '/d/hazards/:id'],
  include: [{
    model: database.TaskModel,
    as: 'task'
  }]
});

// intialize database and listen for requests
database.init()
  .then(() => {
    server.listen(2525);
    console.log('listening at localhost port 2525');
  })
  .catch(() => process.exit(2));
