import { ModelOptions, ModelAttributes } from 'sequelize';

export class ModelDefinition {

  constructor (public readonly attributes: ModelAttributes, public readonly options?: ModelOptions) { }

}
