import { DataTypes } from 'sequelize';

import { ModelDefinition } from './model-definition';

export const Hazard = new ModelDefinition(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    createdBy: {
      type: DataTypes.STRING(64),
      allowNull: false
    },
    hazard: DataTypes.STRING(1024),
    control: DataTypes.STRING(1024),
    severity: DataTypes.SMALLINT
  }
);
