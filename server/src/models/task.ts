import { DataTypes } from 'sequelize';

import { ModelDefinition } from './model-definition';

export const Task = new ModelDefinition(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false
    },
    desc: DataTypes.STRING(1024),
    place: DataTypes.STRING(64),
    equipment: DataTypes.STRING(64)
  }
);
