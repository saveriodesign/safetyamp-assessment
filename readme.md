# Job Hazard Analysis

This project creates a simple PostgreSQL database for drafting, managing, and editing
Job Hazard Analyses.

## Installation and Start-up

### Requirements

Component | My Version
---|:---:
Node | 12.18.2
Typescript | 4.0.2
Angular CLI | 10.0.8
PostgreSQL | 12.4

### Steps

First make sure you have an instance of postgres running

Create a new, blank database by the name `jha-db`. Take note of the owner you've set

Run these commands

```sh
git clone https://gitlab.com/saveriodesign/safetyamp-assessment.git
cd safetyamp-assessment/server
npm i
tsc
# use the real values
npm run populate-db -- -u <owner> -p <password> -a tcp://<postgres-address>:<postgres-port>
npm start -- -u <owner> -p <password> -a tcp://<postgres-address>:<postgres-port>
```

In another terminal, run these commands

```sh
cd safetyamp-assessment/ui
npm i
ng serve
```

At this point the backend API will be running at `http://localhost:2525` and the UI
will be served at the Angular default `http://localhost:4200`.

## Tables

### tasks

Column | Type | Description
---|---|---
id | int4 | Primary Key
name | varchar(64) | Unique title or name for this task
place | varchar(64) | Facility or location for the task
equipment | varchar(64) | Primary piece of dangerous equipment used in this task
desc | varchar(1024) | Thorough description of the task
createdAt | timestamptz | Time of original creation
updatedAt | timestamptz | Time of most-recent edit

### hazards

Column | Type | Description
---|---|---
id | int4 | Primary Key
taskId | int4 | Foreign Key to the "tasks" table by column "id"
name | varchar(64) | Title or name for this task
severity | int2 | Numeric severity rating, 0-4, where 0 kills or dismembers and 4 needs a first-aid kit
hazard | varchar(1024) | Description of the hazard's potential causes and consequences
control | varchar(1024) | Enumeration of hazard controls
createdBy | varchar(64) | Name of the original author
createdAt | timestamptz | Time of original creation
updatedAt | timestamptz | Time of most-recent edit

## API

Fairly cookie-cutter RESTful API. "hazards" is served from `http://localhost:2525/d/hazards`
and "tasks" from `http://localhost:2525/d/tasks`.

GET of an endpoint alone will fetch the whole table.

POST of an endpoint alone will create a new
entry based on the data passed in a JSON body. Failure to provide correct or required data will
respond 400, with a JSON body describing the particular insult.

Adding an integer ID to the end will grant access to that specific resource. GET, PUT, and DELETE are
all permitted.

Query strings may be appended to the URI to GET matching rows, ie `.../d/tasks?place=warehouse` to
fetch all warehouse tasks.

"hazards" will always repond with data which includes the associated Task as a property named `task`.
