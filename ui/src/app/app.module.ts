import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RestService } from './service/rest.service';

import { TaskListItemComponent } from './components/task-list-item/task-list-item.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { HazardListComponent } from './components/hazard-list/hazard-list.component';
import { HazardListItemComponent } from './components/hazard-list-item/hazard-list-item.component';
import { HazardFormComponent } from './components/hazard-form/hazard-form.component';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { SeverityIconComponent } from './components/severity-icon/severity-icon.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    TaskListItemComponent,
    TaskListComponent,
    HazardListComponent,
    HazardListItemComponent,
    HazardFormComponent,
    TaskFormComponent,
    SeverityIconComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
