import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HazardListComponent } from './components/hazard-list/hazard-list.component';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { HazardFormComponent } from './components/hazard-form/hazard-form.component';
import { TaskListComponent } from './components/task-list/task-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/tasks', pathMatch: 'full' },
  { path: 'tasks', component: TaskListComponent },
  { path: 'tasks/new', component: TaskFormComponent },
  { path: 'tasks/view/:id', component: TaskFormComponent },
  { path: 'hazards', component: HazardListComponent },
  { path: 'hazards/new/:id', component: HazardFormComponent },
  { path: 'hazards/for/:id', component: HazardListComponent },
  { path: 'hazards/view/:id', component: HazardFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
