import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Task } from '../../models/models.module';
import { RestService } from '../../service/rest.service';

@Component({
  selector: 'app-task-list-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.css']
})
export class TaskListItemComponent implements OnInit {

  /** Task data */
  @Input() data: Task;
  /** Parent accordion element ID */
  @Input() parentId: string;
  /** Emitter for when associated data is removed from the DB */
  @Output() destroyed = new EventEmitter<number>();

  constructor(private api: RestService) { }

  ngOnInit(): void {
  }

  /** Delete associated row from the DB and notify parent component */
  public delete(): void {
    this.api.deleteTask(this.data.id).subscribe(() => this.destroyed.emit(this.data.id));
  }

}
