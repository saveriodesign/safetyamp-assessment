import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { flatMap } from 'rxjs/operators';

import { Hazard } from '../../models/models.module'
import { RestService } from '../../service/rest.service';

@Component({
  selector: 'app-hazard-list',
  templateUrl: './hazard-list.component.html',
  styleUrls: ['./hazard-list.component.css']
})
export class HazardListComponent implements OnInit {

  /** Full hazard list */
  public data: Array<Hazard>
  /** Task ID of all listed hazards (if filtered) */
  public task: number;

  constructor(
    private router: ActivatedRoute,
    private api: RestService
  ) { }

  ngOnInit(): void {
    this.router.params.pipe(
      flatMap((parameters) => {
        // either fetch all hazards or just the ones for the indicated task
        if (parameters.id) {
          this.task = parameters.id;
          return this.api.searchHazards({taskId: parameters.id});
        }
        else return this.api.listHazards();
      })
    ).subscribe((hazards) => this.data = hazards.filter((h) => !!h.taskId));
  }

  /** Deletes a hazard from the accordion when child deletes its row from the DB */
  public removeItem(id: number): void {
    this.data = this.data.filter((task) => task.id !== id);
  }

}
