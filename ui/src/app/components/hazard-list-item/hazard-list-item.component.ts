import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Hazard } from '../../models/models.module';
import { RestService } from '../../service/rest.service';

@Component({
  selector: 'app-hazard-list-item',
  templateUrl: './hazard-list-item.component.html',
  styleUrls: ['./hazard-list-item.component.css']
})
export class HazardListItemComponent implements OnInit {

  /** Hazard data */
  @Input() data: Hazard;
  /** Parent accordion element ID */
  @Input() parentId: string;
  /** Child-to-Parent event for removal of associated row in the DB */
  @Output() destroyed = new EventEmitter<number>();

  constructor(private api: RestService) { }

  ngOnInit(): void {
  }

  /** Delete this hazard */
  public delete() {
    this.api.deleteHazard(this.data.id).subscribe(() => this.destroyed.emit(this.data.id));
  }

}
