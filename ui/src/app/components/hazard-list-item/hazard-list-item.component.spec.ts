import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HazardListItemComponent } from './hazard-list-item.component';

describe('HazardListItemComponent', () => {
  let component: HazardListItemComponent;
  let fixture: ComponentFixture<HazardListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HazardListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HazardListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
