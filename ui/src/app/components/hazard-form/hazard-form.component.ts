import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { of, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { RestService } from '../../service/rest.service'
import { Hazard, Task, severityMap } from '../../models/models.module'

@Component({
  selector: 'app-hazard-form',
  templateUrl: './hazard-form.component.html',
  styleUrls: ['./hazard-form.component.css']
})
export class HazardFormComponent implements OnInit {

  /** Behave as if we are editing or creating */
  private isNewHazard: boolean;

  /** Task associated with this Hazard */
  public task: Task = {};
  /** Hazard data */
  public hazard: Hazard = {};
  /** Imported severity map */
  public readonly severityMap = severityMap;
  /** Form data */
  public form = new FormGroup({
    name: new FormControl(this.hazard.name, [
      Validators.required,
      Validators.maxLength(64)
    ]),
    severity: new FormControl(this.hazard.severity),
    description: new FormControl(this.hazard.hazard, [
      Validators.maxLength(1024)
    ]),
    control: new FormControl(this.hazard.control, [
      Validators.maxLength(1024)
    ]),
    author: new FormControl(this.hazard.createdBy, [
      Validators.required,
      Validators.maxLength(64)
    ]),
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private api: RestService
  ) { }

  ngOnInit(): void {
    /** URL parameters */
    let parameters: Params;
    this.route.url.pipe(
      flatMap((url) => {
        // determine if this is a new hazard from URL
        if (url.find((segment) => segment.path === 'new')) this.isNewHazard = true;
        else this.isNewHazard = false;
        return this.route.params;
      }),
      flatMap((params) => {
        // fetch hazard if it exists
        parameters = params;
        if (this.isNewHazard) return of({} as Hazard);
        else return this.api.getHazard(parameters.id);
      }),
      flatMap((hazard) => {
        // fetch task if we didn't get it from the hazard
        this.hazard = hazard;
        if (this.isNewHazard) return this.api.getTask(parameters.id);
        else return of(this.hazard.task);
      })
    ).subscribe((task) => this.task = task);

  }

  /** Cancel editing */
  public cancel(): void {
    this.location.back();
  }

  /** Submit changes and return to hazard list for this task */
  public submit(): void{

    let operation: Observable<Hazard>;
    
    if (!this.isNewHazard) {
      operation = this.api.updateHazard(this.hazard.id, {
        name: this.hazard.name,
        severity: this.hazard.severity,
        hazard: this.hazard.hazard,
        control: this.hazard.control,
      });
    }
    else {
      operation = this.api.createHazard({
        name: this.hazard.name,
        severity: this.hazard.severity,
        hazard: this.hazard.hazard,
        control: this.hazard.control,
        createdBy: this.hazard.createdBy,
        taskId: this.task.id
      });
    }

    operation.subscribe((hazard) => this.router.navigate([`/hazards/for/${this.task.id}`]));

  }

}
