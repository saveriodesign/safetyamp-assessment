import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { of, empty, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { RestService } from '../../service/rest.service';
import { Task } from '../../models/models.module';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {

  /** Task data */
  public task: Task = {};

  /** Form controls */
  public form = new FormGroup({
    name: new FormControl(this.task.name, [
      Validators.required,
      Validators.maxLength(64)
    ]),
    place: new FormControl(this.task.place, [
      Validators.maxLength(64)
    ]),
    equipment: new FormControl(this.task.equipment, [
      Validators.maxLength(64)
    ]),
    desc: new FormControl(this.task.desc, [
      Validators.maxLength(1024)
    ])
  })

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: RestService
  ) { }

  ngOnInit(): void {
    this.route.url.pipe(
      flatMap((url) => {
        // determine if this is a new task
        if (url.find((segment) => segment.path === 'new')) return of({} as Params);
        else return this.route.params;
      }),
      flatMap((params) => {
        // fetch task if it should exist
        if (params.id) return this.api.getTask(params.id);
        else return empty();
      })
    ).subscribe((task) => this.task = task);
  }

  /** Submit changes and navigate back to the task list */
  public submit() {

    let operation: Observable<Task>;

    // update
    if (this.task.id) {
      operation = this.api.updateTask(this.task.id, {
        name: this.task.name,
        place: this.task.place,
        equipment: this.task.equipment,
        desc: this.task.desc
      });
    }

    // create
    else {
      operation = this.api.createTask({
        name: this.task.name,
        place: this.task.place,
        equipment: this.task.equipment,
        desc: this.task.desc
      });
    }

    operation.subscribe((task) => this.router.navigate(['/tasks']));

}

}
