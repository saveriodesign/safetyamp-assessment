import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeverityIconComponent } from './severity-icon.component';

describe('SeverityIconComponent', () => {
  let component: SeverityIconComponent;
  let fixture: ComponentFixture<SeverityIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeverityIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeverityIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
