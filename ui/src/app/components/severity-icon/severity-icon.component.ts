import { Component, OnInit, Input } from '@angular/core';

import { severityMap } from '../../models/models.module';

@Component({
  selector: 'app-severity-icon',
  templateUrl: './severity-icon.component.html',
  styleUrls: ['./severity-icon.component.css']
})
export class SeverityIconComponent implements OnInit {

  /** Numeric severity */
  @Input() index: number;

  /** Get the emoji to display from the map */
  public getIcon(): string {
    return severityMap[this.index].icon;
  }

  /** Get the tooltip text to display */
  public getDesc(): string {
    return severityMap[this.index].text;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
