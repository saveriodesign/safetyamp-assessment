import { Component, OnInit } from '@angular/core';

import { Task } from '../../models/models.module'
import { RestService } from '../../service/rest.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  /** Task data */
  public data: Array<Task>;

  constructor(private api: RestService) { }

  ngOnInit(): void {
    this.api.listTasks().subscribe(
      (list) => this.data = list,
      (error) => console.error(error)
    );
  }

  /** Listener for child components removing their data from the DB */
  public removeItem(id: number): void {
    this.data = this.data.filter((task) => task.id !== id);
  }

}
