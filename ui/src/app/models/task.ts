import { Hazard } from './hazard';

export interface Task {
  /** Unique ID */
  id?: number;
  /** Unique name/title of the task (max-length 64) */
  name?: string;
  /** Location of the task (max-length 64) */
  place?: string;
  /** Primary equipment used in the task (max-length 64) */
  equipment?: string;
  /** Thorough description of the task (max-length 1024) */
  desc?: string;
  /** Original creation time of the task */
  createdAt?: Date;
  /** Time the task was last updated */
  updatedAt?: Date;
  /** Hazards of this task */
  hazards?: Array<Hazard>;
}
