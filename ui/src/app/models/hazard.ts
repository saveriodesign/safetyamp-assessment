import { Task } from './task';

export interface Hazard {
  /** Unique ID */
  id?: number;
  /** Unique name/title of the hazard (max-length 64) */
  name?: string;
  /** ID of the task with which this hazard is associated */
  taskId?: number;
  /** Severity level of this hazard */
  severity?: number;
  /** Description of the hazard (max-length 1024) */
  hazard?: string;
  /** Description of hazard controls (max-length 1024) */
  control?: string;
  /** Original author of this hazard (max-length 64) */
  createdBy?: string;
  /** Original creation time of the task */
  createdAt?: Date;
  /** Time the task was last updated */
  updatedAt?: Date;
  /** Parent task */
  task?: Task;
}
