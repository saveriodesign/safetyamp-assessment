/**
 * Map of severity values (index) to displayed data
 * 
 * `0` Death, missing limbs, etc  
 * `1` Workman's Comp scenario, extended time off, full recovery expected  
 * `2` May be hospital-worthy, may be able to work next day  
 * `3` On-site first-aid kit likely sufficient to treat
 */
export const severityMap = [
  {
    icon: '&#10071;&#10071;',
    text: 'Potential for Permanently-Disabling Injury or Death'
  },
  {
    icon: '&#10071;',
    text: 'Potential for Serious Injury'
  },
  {
    icon: '!',
    text: 'Potential for Moderate Injury'
  },
  {
    icon: '~',
    text: 'Potential for Minor Injury'
  }
];
