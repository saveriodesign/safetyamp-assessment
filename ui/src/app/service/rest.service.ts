import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { environment as env } from '../../environments/environment';
import { Task, Hazard } from '../models/models.module';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  /** Fetch a specific Task by its ID */
  public getTask(id: number): Observable<Task> {
    return this.http.get<Task>(`${env.apiHost}/tasks/${id}`);
  }
  
  /** Fetch all Tasks */
  public listTasks(): Observable<Array<Task>> {
    return this.http.get<Array<Task>>(`${env.apiHost}/tasks`);
  }

  /** Create a Task */
  public createTask(data: Task): Observable<Task> {
    return this.http.post<Task>(`${env.apiHost}/tasks`, data);
  }

  /** Update a Task */
  public updateTask(id: number, data: Task): Observable<Task> {
    return this.http.put<Task>(`${env.apiHost}/tasks/${id}`, data);
  }

  /** Delete a Task */
  public deleteTask(id: number): Observable<any> {
    return this.http.delete(`${env.apiHost}/tasks/${id}`);
  }

  /** Search for matching Tasks */
  public searchTasks(data: Task): Observable<Array<Task>> {
    let queryString = '';
    for (let key in data) queryString = queryString.concat(key, '=', data[key]);
    return this.http.get<Array<Task>>(`${env.apiHost}/tasks?${queryString}`);
  }

  /** Fetch a specific Hazard by its ID */
  public getHazard(id: number): Observable<Hazard> {
    return this.http.get<Hazard>(`${env.apiHost}/hazards/${id}`);
  }

  /** Fetch all Hazards */
  public listHazards(): Observable<Array<Hazard>> {
    return this.http.get<Array<Hazard>>(`${env.apiHost}/hazards`);
  }

  /** Create a Hazard */
  public createHazard(data: Hazard): Observable<Hazard> {
    return this.http.post<Hazard>(`${env.apiHost}/hazards`, data);
  }

  /** Update a Hazard */
  public updateHazard(id: number, data: Hazard): Observable<Hazard> {
    return this.http.put<Hazard>(`${env.apiHost}/hazards/${id}`, data);
  }

  /** Delete a Hazard */
  public deleteHazard(id: number): Observable<any> {
    return this.http.delete(`${env.apiHost}/hazards/${id}`);
  }

  /** Search for matching Hazards */
  public searchHazards(data: Hazard): Observable<Array<Hazard>> {
    let queryString = '';
    for (let key in data) queryString = queryString.concat(key, '=', data[key]);
    return this.http.get<Array<Hazard>>(`${env.apiHost}/hazards?${queryString}`);
  }

}
