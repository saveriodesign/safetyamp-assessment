module.exports = [
	{
		"id" : 2,
		"name" : "Move Inventory",
		"desc" : "Worker operates forklift to either\n(a) Move pallets from trucks at Receiving Bay to Storage Racks throughout the Warehouse\n(b) Move pallets from Storage Racks to Manufacturing",
		"place" : "Warehouse",
		"equipment" : "Forklift",
		"createdAt" : "2020-09-02T13:35:34.263Z",
		"updatedAt" : "2020-09-02T13:40:32.826Z"
	},
	{
		"id" : 3,
		"name" : "De-burr Aluminum Stock",
		"desc" : "1. Worker lifts a (4 lb) bar of cut aluminum stock from a box by the grinder\n2. Worker grinds the cut ends of the bar on all edges to remove burrs\n3. Worker places the de-burred stock into another box",
		"place" : "Machine Shop",
		"equipment" : "Belt Grinder",
		"createdAt" : "2020-09-02T14:53:24.388Z",
		"updatedAt" : "2020-09-02T14:53:24.388Z"
	}
];
