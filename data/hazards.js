module.exports = [
	{
		"id" : 4,
		"createdBy" : "Billy Jean",
		"hazard" : "A warehouse worker may be struck by either the forklift or its payload",
		"control" : "1. Implement 5 mph speed limit in warehouse\n2. Assign spotter to assist operator and keep other workers at a safe distance\n3. Lay down hazard tape outlining common forklift routes",
		"severity" : 1,
		"createdAt" : "2020-09-02T14:10:58.864Z",
		"updatedAt" : "2020-09-02T14:10:58.864Z",
		"taskId" : 2,
		"name" : "Strike Pedestrian"
	},
	{
		"id" : 7,
		"createdBy" : "Paul Rudd",
		"hazard" : "Workers hand or arm comes in contact with the grinder, quickly burning off skin. Degloving possible in extreme cases",
		"control" : "1. Worker dons well-fitting heavy-duty gloves\n2. Worker should maintain proper balance, and never leaning their body weight to accelerate grinding",
		"severity" : 2,
		"createdAt" : "2020-09-02T14:56:30.843Z",
		"updatedAt" : "2020-09-02T14:56:30.843Z",
		"taskId" : 3,
		"name" : "Abrasion Injury"
	},
	{
		"id" : 8,
		"createdBy" : "Paul Rudd",
		"hazard" : "Burrs are jagged and sharp, and may slice the skin of the worker.",
		"control" : "1. Worker grips unprocessed bars by the shaft, well away from the ends\n2. Worker dons well-fitting heavy-duty gloves",
		"severity" : 3,
		"createdAt" : "2020-09-02T15:02:45.973Z",
		"updatedAt" : "2020-09-02T15:02:45.973Z",
		"taskId" : 3,
		"name" : "Laceration"
	},
	{
		"id" : 6,
		"createdBy" : "Samwise Gamgee",
		"hazard" : "By either a payload imbalance or a gap between the docking bay and delivery truck, the forklift or its payload may fall upon a worker.",
		"control" : "1. Operator should always fasten 5-point harness\n2. Assigned spotter should help guide the operator into and out of delivery trucks\n3. Lay down hazard tape outlining common forklift routes\n4. Docking bay bridge position and stability should be independently confirmed by two workers\n5. Operator should drive toward the payload until the pallet is touching the backplate of the fork before lifting\n6. Payloads should be centered upon their pallets",
		"severity" : 0,
		"createdAt" : "2020-09-02T14:47:58.349Z",
		"updatedAt" : "2020-09-02T15:06:37.331Z",
		"taskId" : 2,
		"name" : "Rollover"
	},
	{
		"id" : 10,
		"createdBy" : "asdfasdf",
		"hazard" : "gasdg",
		"control" : "fasdfasd",
		"severity" : 2,
		"createdAt" : "2020-09-02T16:04:12.983Z",
		"updatedAt" : "2020-09-02T16:04:12.983Z",
		"taskId" : null,
		"name" : "asdgas"
	}
];
